# Twemped Redis


Creates an alpine based image with twemproxy and 4 instances of redis
- One redis instance master, that listen on local port 6379
- Three redis instances slave, that listening on local ports 6380-6382
- Twemproxy configured with services alpha and beta
- Alpha, proxies in slaves in readonly mode and listen on exposed port 5001
- Beta, proxies in master in readwrite mode and listen on exposed port 5002


### Installation


```sh
docker run  -p 5001:5001 -p 5002:5002 -itd saimirballeshi/twemped_redis
```

